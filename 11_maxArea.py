class Solution:
    def maxArea(self, height) -> int:
        length = len(height)
        start = 0
        end = length - 1
        max_area = 0
        while start < end:
            if height[start] < height[end]:
                area = (end-start) * height[start]
                start += 1
            else:
                area = (end-start) * height[end]
                end -= 1
            max_area = max(max_area, area)
        return max_area

solution = Solution()
height = [1, 2, 1]
print(solution.maxArea(height))
