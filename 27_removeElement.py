from typing import List
class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        length = len(nums)
        fast = 0
        slow = 0
        while fast < length:
            if not nums[fast] == val:
                nums[slow] = nums[fast]
                slow += 1
            fast += 1
        return slow

nums = [3,2,2,3]
val = 3
solution = Solution()
print(solution.removeElement(nums, val), nums)
