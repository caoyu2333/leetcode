class Solution:
    def longestPalindrome(self, s: str) -> str:
        length = len(s)
        max_length = 0
        palindrome = ''
        if length < 2:
            return s

        matrix = [[False] * length for i in range(length)]
        for i in range(length):
            matrix[i][i] = True  # 字符自己跟自己回文

        for L in range(2, length+1):
            for i in range(length):
                j = i + L - 1
                if j >= length:
                    break
                if s[i] == s[j]:
                    if L == 2:
                        matrix[i][j] = True
                    else:
                        matrix[i][j] = matrix[i+1][j-1]
                    if matrix[i][j] and L > max_length:
                        max_length = L
                        palindrome = s[i:j+1]
        if max_length == 0:
            palindrome = s[0]

        return palindrome

s = 'ab'
n = Solution()
print(n.longestPalindrome(s))





