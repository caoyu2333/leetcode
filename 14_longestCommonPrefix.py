class Solution:
    def longestCommonPrefix(self, strs) -> str:
        index = 0
        prefix = ''
        flag = True
        while flag:
            try:
                current_str = strs[0][index]
                for i in range(1, len(strs)):
                    if not strs[i][index] == current_str:
                        flag = False
                if flag:
                    prefix += current_str
                    index += 1
            except:
                break
        return prefix


solution = Solution()
strs = ["dog","racecar","car"]
print(solution.longestCommonPrefix(strs))



