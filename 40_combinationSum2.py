from typing import List
class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        candidates.sort()
        result = []
        path = []

        def back(idx, target):
            if target == 0:
                result.append(path[:])
            elif target < 0 or idx >= len(candidates):
                return
            else:
                for i in range(idx, len(candidates)):
                    if i > idx and candidates[i] == candidates[i-1]:
                        continue
                    path.append(candidates[i])
                    back(i+1, target-candidates[i])
                    path.pop()
        back(0, target)
        return result

candidates = [10,1,2,7,6,1,5]
target = 8
solution = Solution()
print(solution.combinationSum(candidates, target))
