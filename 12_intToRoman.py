class Solution:
    def intToRoman(self, num: int) -> str:
        rome = ''
        if num >= 1000:
            rome += 'M' * (num // 1000)
            num = num % 1000

        if num >= 500:
            if num >= 900:
                rome += 'CM'
                num -= 900
            else:
                rome += 'D'
                num -= 500

        if num >= 100:
            if num >= 400:
                rome += 'CD'
            else:
                rome += 'C' * (num // 100)
            num = num % 100

        if num >= 50:
            if num >= 90:
                rome += 'XC'
                num -= 90
            else:
                rome += 'L'
                num -= 50

        if num >= 10:
            if num >= 40:
                rome += 'XL'
            else:
                rome += 'X' * (num // 10)
            num = num % 10

        if num >= 5:
            if num == 9:
                rome += 'IX'
                num -= 9
            else:
                rome += 'V'
                num -= 5

        if num == 4:
            rome += 'IV'
        else:
            rome += 'I' * num

        return rome

solution = Solution()
num = 1994
print(solution.intToRoman(num))




