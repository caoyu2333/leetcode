from typing import List
class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        length = len(nums)
        result = []
        def backtrace(idx):
            if idx == length-1:
                result.append(nums[:])
            else:
                for i in range(idx, length):
                    nums[idx], nums[i] = nums[i], nums[idx]
                    backtrace(idx+1)
                    nums[idx], nums[i] = nums[i], nums[idx]
        backtrace(0)
        return result

solution = Solution()
nums = [1,2,3]
print(solution.permute(nums))

