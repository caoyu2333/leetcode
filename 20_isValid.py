class Solution:
    def isValid(self, s: str) -> bool:
        my_dict = {'(': ')', '{': '}', '[': ']'}
        my_list = []
        for string in s:
            if string == '(' or string == '{' or string == '[':
                my_list.append(string)
            else:
                try:
                    if not my_dict[my_list.pop()] == string:
                        return False
                except:
                    return False
        if len(my_list) > 0:
            return False
        else:
            return True

solution = Solution()
s = "([)]"
print(solution.isValid(s))