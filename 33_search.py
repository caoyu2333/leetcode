from typing import List
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        start = 0
        end = len(nums) - 1
        if end == 0:
            if nums[0] == target:
                return 0
            else:
                return -1
        while end > start:
            middle = (start + end) // 2
            if nums[middle] > nums[start]:
                if target > nums[middle]:
                    start = middle
                elif target < nums[middle]:
                    if target < nums[start]:
                        start = middle
                    else:
                        end = middle
                else:
                    return middle
            elif nums[middle] < nums[start]:
                if target > nums[middle]:
                    if target > nums[end]:
                        end = middle
                    else:
                        start = middle
                elif target < nums[middle]:
                    end = middle
                else:
                    return middle
            else:
                if target == nums[start]:
                    return start
                elif target == nums[end]:
                    return end
                else:
                    return -1



nums = [1]
target = 3
solution = Solution()
print(solution.search(nums, target))







