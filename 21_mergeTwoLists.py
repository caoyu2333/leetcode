# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def list2listnode(list):
    if len(list) == 0:
        return None
    head = temp = ListNode()
    for i in range(len(list)):
        temp.val = list[i]
        if not i == len(list) - 1:
            temp.next = ListNode()
            temp = temp.next
    return head


class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        dummy = ListNode()
        temp = dummy

        while l1 and l2:
            if l1.val < l2.val:
                temp.next = l1
                l1 = l1.next
            else:
                temp.next = l2
                l2 = l2.next
            temp = temp.next
        if l1:
            temp.next = l1
        if l2:
            temp.next = l2
        return dummy.next

l1 = list2listnode([1, 2, 3])

l2 = list2listnode([4])
solution = Solution()
head = solution.mergeTwoLists(l1, l2)
while head:
    print(head.val)
    head = head.next