def twoSum(nums, target):
    my_dict = {x: [] for x in nums}
    for index, num in enumerate(nums):
        my_dict[num].append(index)

    for key in my_dict.keys():
        try:
            if len(my_dict[target - key]) > 0:
                if target - key == key:
                    if len(my_dict[target - key]) > 1:
                        return my_dict[key][:2]
                else:
                    return [my_dict[key][0], my_dict[target - key][0]]
        except:
            pass


if __name__ == '__main__':
    nums = [2, 5, 5, 11]
    print(twoSum(nums, 10))
