class Solution:
    def isPalindrome(self, x: int) -> bool:
        temp = x
        if x < 0:
            return False
        reversed_num = 0
        while x:
            reversed_num = reversed_num * 10 + x % 10
            x = x // 10
        print(reversed_num)
        return temp == reversed_num

solution = Solution()
num = 121
print(solution.isPalindrome(num))