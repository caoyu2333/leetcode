class AutoMachine():
    def __init__(self):
        self.current_state = 'start'
        self.sign = 1
        self.num = 0

        self.state_matrix = {
            'start': ['start', 'sign', 'num', 'end'],
            'sign': ['end', 'end', 'num', 'end'],
            'num': ['end', 'end', 'num', 'end'],
            'end': ['end', 'end', 'end', 'end']
        }

    def state_trans(self, s: str):
        if s.isspace():
            return 0
        elif s == '+' or s == '-':
            return 1
        elif s.isdigit():
            return 2
        else:
            return 3

    def get_num(self, s):
        for i in range(len(s)):
            # print(s[i].isdigit())
            # print(self.current_state)
            # print(self.state_trans(s[i]))
            self.current_state = self.state_matrix[self.current_state][self.state_trans(s[i])]

            if self.current_state == 'end':
                break
            elif self.current_state == 'sign':
                if s[i] == '+':
                    self.sign = 1
                else:
                    self.sign = -1
            elif self.current_state == 'num':
                self.num = self.num * 10 + int(s[i])

        if self.sign == 1:
            return min(self.num, 2**31-1)
        else:
            return max(self.num*self.sign, -2**31)

class Solution:
    def myAtoi(self, s: str) -> int:
        auto_machine = AutoMachine()
        return auto_machine.get_num(s)


solution = Solution()
print(solution.myAtoi('     -+43 asd'))

