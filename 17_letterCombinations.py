class Solution:
    def letterCombinations(self, digits: str):
        def two_combine(list1, list2):
            temp = []
            for i in list1:
                for j in list2:
                    temp.append(i+j)
            return temp

        my_dict = {
            '2': ['a', 'b', 'c'],
            '3': ['d', 'e', 'f'],
            '4': ['g', 'h', 'i'],
            '5': ['j', 'k', 'l'],
            '6': ['m', 'n', 'o'],
            '7': ['p', 'q', 'r', 's'],
            '8': ['t', 'u', 'v'],
            '9': ['w', 'x', 'y', 'z']
        }
        num_list = []
        if len(digits) == 0:
            return []

        for string in digits:
            num_list.append(my_dict[string])

        if len(num_list) == 1:
            return num_list[0]
        else:
            result = num_list[0]
            for i in range(1, len(num_list)):
                result = two_combine(result, num_list[i])
        return result

solution = Solution()
digits = "2"
print(solution.letterCombinations(digits))


