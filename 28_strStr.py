class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        if len(needle) == 0:
            return 0
        else:
            i = 0
            j = 0
            while i <= len(haystack) - len(needle):
                if haystack[i] == needle[j]:
                    flag = True
                    while j < len(needle)-1:
                        j += 1
                        if i+j >= len(haystack) or not haystack[i+j] == needle[j]:
                            flag = False
                            j = 0
                            break
                    if flag:
                        return i

                i += 1
            return -1


solution = Solution()
haystack = "mississippi"

needle = "issip"
print(solution.strStr(haystack, needle))
