from typing import List
class Solution:
    def nextPermutation(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        length = len(nums)
        i = length - 2
        while i > 0 and nums[i] >= nums[i+1]:
            i -= 1
        j = length - 1
        while j > i:
            if nums[i] >= nums[j]:
                j -= 1
            else:
                break
        if j > i:
            nums[j], nums[i] = nums[i], nums[j]
        else:
            i = -1

        left = i + 1
        right = length - 1
        while left < right:
            nums[left], nums[right] = nums[right], nums[left]
            left += 1
            right -= 1


nums = [2,2,7,5,4,3,2,2,1]
solution = Solution()
solution.nextPermutation(nums)
print(nums)