class Solution:
    def romanToInt(self, s: str) -> int:
        my_dict = {
            'M': 1000,
            'CM': 900,
            'D': 500,
            'CD': 400,
            'C': 100,
            'XC': 90,
            'L': 50,
            'XL': 40,
            'X': 10,
            'IX': 9,
            'V': 5,
            'IV': 4,
            'I': 1
        }
        num = 0
        i = 0
        while i < len(s):
            if (s[i] == 'C' and (not i == len(s)-1) and (s[i+1] == 'D' or s[i+1] == 'M')) \
                    or (s[i] == 'X' and (not i == len(s)-1) and (s[i+1] == 'L' or s[i+1] == 'C')) \
                    or (s[i] == 'I' and (not i == len(s)-1) and (s[i+1] == 'V' or s[i+1] == 'X')):
                num += my_dict[s[i:i+2]]
                i += 2
            else:
                num += my_dict[s[i]]
                i += 1
        return num

solution = Solution()
rome = "MDLXX"
print(solution.romanToInt(rome))

