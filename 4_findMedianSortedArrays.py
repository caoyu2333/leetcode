"""
给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
"""

class Solution:
    def findMedianSortedArrays(self, nums1, nums2):
        index1 = 0
        index2 = 0
        new_array = []
        length1 = len(nums1)
        length2 = len(nums2)
        counter = 0
        if (length1+length2) % 2 == 0:
            end_num = (length1+length2) // 2 + 1
            odd = False
        else:
            end_num = (length1+length2+1) // 2
            odd = True
        nums1.append(-1)
        nums2.append(-1)

        while counter < end_num:
            flag1 = (nums1[index1] > nums2[index2])
            print(index1, index2)
            if flag1:
                if index2 < length2:
                    new_array.append(nums2[index2])
                    index2 += 1
                else:
                    new_array.append(nums1[index1])
                    index1 += 1

            else:
                if index1 < length1:
                    new_array.append(nums1[index1])
                    index1 += 1
                else:
                    new_array.append(nums2[index2])
                    index2 += 1
            counter += 1

        if odd:
            return new_array[-1]
        else:
            return (new_array[-1] + new_array[-2]) / 2


# class Solution:
#     def findMedianSortedArrays(self, nums1, nums2):
#         def find_k(list1, list2, k):
#             print(list1, list2, k)
#             if len(list1) == 0:
#                 return list2[k - 1]
#             if len(list2) == 0:
#                 return list1[k - 1]
#             if k == 1:
#                 return min(list1[0], list2[0])
#
#             index1 = max(len(list1) // 2 - 1, 0)
#             index2 = max(len(list2) // 2 - 1, 0)
#
#             if list1[index1] <= list2[index2]:
#                 k = k - index1 - 1
#                 list1 = list1[index1+1:]
#             elif list1[index1] > list2[index2]:
#                 k = k - index2 - 1
#                 list2 = list2[index2+1:]
#
#             return find_k(list1, list2, k)
#
#         len1 = len(nums1)
#         len2 = len(nums2)
#         if (len1 + len2) % 2 == 1:
#             return find_k(nums1, nums2, int((len1 + len2 + 1) / 2))
#         else:
#             return (find_k(nums1, nums2, int((len1 + len2) / 2)) + find_k(nums1, nums2, int((len1 + len2) / 2 + 1))) / 2


nums1 = [2, 2, 4, 4]
nums2 = [2, 2, 4, 4]
n = Solution()
print(n.findMedianSortedArrays(nums1, nums2))


