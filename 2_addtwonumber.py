# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        result = temp = ListNode()
        carry_num = 0
        while l1 or l2 or carry_num:
            if l1:
                carry_num += l1.val
                l1 = l1.next
            if l2:
                carry_num += l2.val
                l2 = l2.next

            temp.val = carry_num % 10
            carry_num = carry_num // 10
            if l1 or l2 or carry_num:
                temp.next = ListNode()
                temp = temp.next

        return result


if __name__ == '__main__':
    n = Solution()
