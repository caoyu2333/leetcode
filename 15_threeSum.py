class Solution:
    def threeSum(self, nums):
        sorted_nums = sorted(nums)
        result = []
        length = len(sorted_nums)
        for start in range(length):

            if start > 0 and sorted_nums[start] == sorted_nums[start-1]:
                continue
            end = length - 1
            target = -sorted_nums[start]
            if target < 0:
                break
            for middle in range(start+1, length):
                if middle > start+1 and sorted_nums[middle] == sorted_nums[middle-1]:
                    continue

                while middle < end and target < sorted_nums[middle] + sorted_nums[end]:
                    end -= 1

                    # print(start, middle, end)

                if not middle == end and target == sorted_nums[middle] + sorted_nums[end]:
                    print(start, middle, end)
                    result.append([sorted_nums[start], sorted_nums[middle], sorted_nums[end]])

        return result


nums = [-1,0,1,2,-1,-4]
solution = Solution()
print(solution.threeSum(nums))
