class Solution:
    def generateParenthesis(self, n: int):
        if n == 0:
            return []
        n_dict = {x: [] for x in range(n+1)}
        n_dict[0] = ['']

        for i in range(1, n+1):
            for p in range(i):
                q = i - p - 1
                for temp1 in n_dict[p]:
                    for temp2 in n_dict[q]:
                        n_dict[i].append('({}){}'.format(temp1, temp2))
        return n_dict[n]

n = 3
solution = Solution()
print(solution.generateParenthesis(n))


















































