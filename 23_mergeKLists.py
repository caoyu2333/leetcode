# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def list2listnode(list):
    if len(list) == 0:
        return None
    head = temp = ListNode()
    for i in range(len(list)):
        temp.val = list[i]
        if not i == len(list) - 1:
            temp.next = ListNode()
            temp = temp.next
    return head


class Solution:
    def mergeKLists(self, lists):
        import numpy as np
        list_num = len(lists)
        end_num = 0
        val_list = []
        dummy = ListNode()
        temp = dummy
        for i in range(list_num):
            if lists[i]:
                val_list.append(lists[i].val)
            else:
                end_num += 1
                val_list.append(10**5)

        val_list = np.array(val_list)
        while end_num < list_num:
            min_index = np.argmin(val_list)
            temp.next = lists[min_index]

            lists[min_index] = lists[min_index].next
            temp = temp.next
            if not lists[min_index]:
                val_list[min_index] = 10**5
                end_num += 1
            else:
                val_list[min_index] = lists[min_index].val

        return dummy.next

a = [1, 4, 5]
b = [1, 3, 4]
c = [2, 6]

lists = [list2listnode(a), list2listnode(b), list2listnode(c)]
solution = Solution()
result = solution.mergeKLists(lists)
while result:
    print(result.val)
    result = result.next






