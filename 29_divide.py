class Solution:
    def divide(self, dividend: int, divisor: int) -> int:
        MAX = 2147483647
        MIN = -2147483648
        pos = (dividend > 0 and divisor > 0) or (dividend < 0 and divisor < 0)
        dividend = abs(dividend)
        divisor = abs(divisor)

        def son_div(dividend, divisor, result):
            temp = 1
            temp_divisor = divisor
            if dividend < temp_divisor:
                return result
            elif dividend == temp_divisor:
                return result + 1
            else:
                while dividend > temp_divisor:
                    temp_divisor = temp_divisor << 1
                    temp = temp << 1
                result += (temp >> 1)
                return son_div(dividend - (temp_divisor >> 1), divisor, result)

        result = son_div(dividend, divisor, 0)

        if not pos:
            return max(MIN, -result)
        else:
            return min(MAX, result)

solution = Solution()
dividend = 1
divisor = 1
print(solution.divide(dividend, divisor))