class Solution:
    def fourSum(self, nums: list, target: int):
        nums.sort()
        length = len(nums)
        result = []
        if length < 4:
            return result
        for i in range(length-3):
            three_target = target - nums[i]
            if i > 0 and nums[i] == nums[i-1]:
                continue
            for j in range(i+1, length-2):
                two_target = three_target - nums[j]
                if j > i+1 and nums[j] == nums[j-1]:
                    continue
                for k in range(j+1, length-1):
                    if k > j+1 and nums[k] == nums[k-1]:
                        continue

                    one_target = two_target - nums[k]
                    m = length-1
                    while nums[m] > one_target and m > k:

                        m -= 1
                    if m == k:
                        continue
                    if nums[m] == one_target:
                        result.append([nums[i], nums[j], nums[k], nums[m]])
        return result

nums = [-2,-1,0,0,1,2]
target = 0
solution = Solution()
print(solution.fourSum(nums, target))

