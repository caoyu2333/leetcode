from typing import List
class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        start, end = 0, len(nums)
        while start < end:
            mid = (start + end) // 2
            if nums[mid] > target:
                end = mid
            elif nums[mid] == target:
                return mid
            else:
                start = mid + 1

        return start

solution = Solution()
nums = []
target = 7
print(solution.searchInsert(nums, target))