class Solution:
    def reverse(self, x: int) -> int:
        reversed_num = 0
        positive_flag = x >= 0
        if not positive_flag:
            x = x * -1
        while x > 0:
            reversed_num = reversed_num * 10 + x % 10
            x = x // 10

        if (positive_flag and reversed_num > 2**31 - 1) or (not positive_flag and reversed_num > 2**31):
            reversed_num = 0
        if not positive_flag:
            reversed_num = reversed_num * -1
        return reversed_num


solution = Solution()
n = 1534
print(solution.reverse(n))
# print(2147483651 - 2**32)

