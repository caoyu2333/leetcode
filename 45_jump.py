from typing import List
class Solution:
    def jump(self, nums: List[int]) -> int:
        num_jump = 0
        start = 0
        end = 1
        max_pos = 0

        while end < len(nums):
            for i in range(start, end):
                max_pos = max(max_pos, nums[i] + i)
            start = end
            end = max_pos + 1
            num_jump += 1
        return num_jump

solution = Solution()
nums = [2,3,0,1,4]
print(solution.jump(nums))

