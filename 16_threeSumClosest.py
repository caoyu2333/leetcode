class Solution:
    def threeSumClosest(self, nums: list, target: int) -> int:
        nums.sort()
        length = len(nums)
        min_dis = 10**6
        print(nums)
        for start in range(length):
            if start > 0 and nums[start] == nums[start-1]:
                continue
            for middle in range(start+1, length):
                end = length - 1
                last_dis = 10 ** 6

                if middle > start + 1 and nums[middle] == nums[middle - 1]:
                    continue

                while middle < end:

                    if middle == end:
                        break
                    new_dis = abs(target-nums[start]-nums[middle]-nums[end])

                    if new_dis > last_dis:
                        break
                    if new_dis < min_dis:
                        min_dis = new_dis
                        result = nums[start] + nums[middle] + nums[end]

                        # print(start, middle, end)
                    last_dis = new_dis
                    end -= 1
        return result

nums = [1,2,4,8,16,32,64,128]
target = 82
solution = Solution()
print(solution.threeSumClosest(nums, target))
