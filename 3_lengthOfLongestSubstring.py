"""
使用滑动窗口来解决
"""
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        from collections import defaultdict
        my_dict = defaultdict(int)
        left = 0
        right = 0
        repeat = False
        max_length = 0

        for i in range(len(s)):
            if my_dict[s[i]] > 0:
                repeat = True    # 说明之前已有该字符，因此重复
            my_dict[s[i]] += 1
            right += 1
            while repeat:
                if my_dict[s[left]] > 1:
                    repeat = False
                my_dict[s[left]] -= 1
                left += 1
            max_length = max(max_length, right-left)

        return max_length

s = "abcabcbb"
n = Solution()
print(n.lengthOfLongestSubstring(s))