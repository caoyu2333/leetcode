from typing import List

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        length = len(nums)
        if length <= 1:
            return length
        else:
            fast = 1
            slow = 1
            while fast < length:
                if not nums[fast] == nums[fast-1]:
                    nums[slow] = nums[fast]
                    slow += 1
                fast += 1
        return slow

nums = [0,0,1,1,1,2,2,3,3,4]
solution = Solution()
print(solution.removeDuplicates(nums), nums)


