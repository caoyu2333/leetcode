class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows == 1:
            return s

        my_dict = {}
        divide_num = 2*numRows-2
        for n in range(numRows):
            my_dict[n] = ''

        for i in range(len(s)):
            n = i % divide_num
            n = min(n, 2*numRows-2-n)
            my_dict[n] += s[i]

        new_string = ''
        for n in range(numRows):
            new_string += my_dict[n]
        return new_string


s = "PAYPALISHIRING"
numRows = 3
solution = Solution()
print(solution.convert(s, numRows))