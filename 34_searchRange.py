from typing import List


class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        if not nums:
            return [-1, -1]

        def binary_search(left, right, target):  # 此处left、right变化方式与判断条件的写法是在查找 “值为target的目标” 的最小下标
            while (left < right):
                mid = (left + right) // 2
                if nums[mid] < target:
                    left = mid + 1
                else:
                    right = mid
            return left

        left, right = 0, len(nums)
        res = []
        res.append(binary_search(left, right, target))  # 查找target的最小下标
        res.append(binary_search(left, right, target + 1) - 1)  # 查找target+1的最小下标，然后-1即为target的最大下标
        if res[0] > res[1]:
            return [-1, -1]
        else:
            return res

solution = Solution()
nums = [1]
target = 1
print(solution.searchRange(nums, target))
