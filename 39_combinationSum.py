from typing import List
class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        result = []
        path = []

        def back(idx, target):
            if target == 0:
                result.append(path[:])
            elif target < 0 or idx >= len(candidates):
                return
            else:
                for i in range(idx, len(candidates)):
                    path.append(candidates[i])
                    back(i, target-candidates[i])
                    path.pop()
        back(0, target)
        return result

candidates = [2,3,6,7]
target = 7
solution = Solution()
print(solution.combinationSum(candidates, target))

